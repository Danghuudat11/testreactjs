import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
export default class ListComponent extends Component{
    constructor(props){
        super(props);
        this.state=({
            data:{
                data:[]
            },
            current_page: 1,
            sort_by:'id',
            type_sort:'asc',
            last_page:'',
            rangeWithDots:[]



        });
        this.handlePrev = this.handlePrev.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.handleChangePage=this.handleChangePage.bind(this);

    }
    componentDidMount() {
        console.log(this.state);
        this.fetchData();
    }
    fetchData(){
        let current=this.state.current_page;
        let sort_by=this.state.sort_by;
        let type_sort=this.state.type_sort;

        axios.get( 'admin/getlist?page='+current+'&sort_by='+sort_by+'&type_sort='+type_sort).then(res=>{
            let data=res.data.data.data;
            let current_page=res.data.data.current_page;
            let last_page=res.data.data.last_page;
            console.log(last_page);
            //1000
            this.setState({
                data:{
                    data:data
                },
                last_page:last_page,
                current_page:current_page,
            })
        });
    }
    handleChangePage(page){
        this.state.current_page=page;

        this.fetchData()
    }
    changeTypeSort(column){
        let type_sort=this.state.type_sort=='asc'?'desc':'asc';
        this.setState({
            sort_by:column,
            type_sort:type_sort
        })
        this.fetchData();

    }
    delete(id){
        axios.get( 'admin/getdelete/'+id).then(res=>{
                /*console.log(res);*/
        });
        this.fetchData();

    }
    handlePrev(){
        if(this.state.current_page!=1){
            this.state.current_page--;
            this.fetchData();
        }

    }
    handleNext(){
        if(this.state.current_page<=this.state.last_page){
            this.state.current_page++;
            this.fetchData();
        }
    }

    pagination() {// c = current_page, m = sum page
        let delta = 2,
            range = [],
            rangeWithDots = [],
            l;
        range.push(1)
        for (let i = this.state.current_page - delta; i <= this.state.current_page + delta; i++) {
            if (i < this.state.last_page && i > 1) {
                range.push(i);
            }
        }
        if (this.state.last_page > 1) {
            range.push(this.state.last_page);
        }
        range.map(val => {
            if (l) {
                if (val - l === 2) {
                    rangeWithDots.push(l + 1);
                } else if (val - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(val);
            l = val;
        });

        return rangeWithDots;
    }

    render(){
        console.log(this.state)
        const dots=this.pagination();
        console.log(dots);
        const elmPagination = dots.map( (val, index) => {
            return (
                <div key={index} className={this.state.current_page === val ? 'active' : ''}>
                    <a onClick={() => this.handleChangePage(val)}>{val}</a>
                </div>
            )
        });

        const data=this.state.data.data;
        const rowData=data.map((item,index)=>{
            return(
                <tr key={index  } id={'data'+index}>
                    <td>{item.id}</td>
                    <td><Link to={'/getdetail/'+item.id}>{item.name}</Link></td>
                    <td>{item.email}</td>
                    <td>{item.phone}</td>
                    <td>{item.address}</td>
                    <td> <button className="btn btn-danger btn-sm"
                                 onClick={() => this.delete(item.id)}
                    >Delete</button></td>
                    <td><Link className="btn btn-danger btn-sm" to={'/getedit/'+item.id} >Edit</Link></td>
                </tr>
            )
        });
        return(
            <div>
                {/*<div>
                    <Link className="btn btn-sm btn-primary" to={'/getdetail/1000'}>1234</Link>
                </div>*/}
                <table className="table table-hover">
                    <thead>
                    <tr>
                        <th  onClick={() => this.changeTypeSort('id')}>ID</th>
                        <th onClick={() => this.changeTypeSort('name')}>NAME</th>
                        <th onClick={() => this.changeTypeSort('email')}>EMAIL</th>
                        <th onClick={() => this.changeTypeSort('phone')}>PHONE</th>
                        <th onClick={() => this.changeTypeSort('address')}>ADDRESS</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    {rowData}

                    </tbody>
                </table>
                <div>
                    <button onClick={() => this.handlePrev()}>Prev</button>
                    {elmPagination}
                    <button onClick={() => this.handleNext()}>Next</button>

                </div>
                <div>
                    <br/>
                    <Link className="btn btn-danger btn-sm" to={'/getadd'} >Add</Link>
                </div>

            </div>



            )
    }
}
