import React, { Component } from 'react';
import axios from 'axios';
import history from './../../history';

export default class FormComponent extends Component{
    constructor(props){
        super(props);
        this.state=({
            data:{
            },
            form: {
                'id':0
            },
            errors:{

            }
        });
        this.change = this.change.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentDidMount() {
        const {mode} = this.props;
        let url='/admin/getadd';

        if(mode==="edit"){
            let id=this.props.match.params.id;
            url='/admin/getdetail/'+id;

            this.fetchData(url);
        }
        this.fetchData(url);

    }
    change(event){
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            form: {
                ...this.state.form, [name]: value,
                id:this.state.data.id,

            }
        });
    }
    fetchData(url){
        axios.get(url).then(res=>{
            this.setState({
                data:res.data.data
            })
        });
    }
    handleSubmit(event){
        /*this.setState({
            form: {
                //id:this.props.match.params.id,
                id:this.state.data.id,
            }
        });*/
        event.preventDefault();
        const form= this.state.form;
        if(form.id!=0&&form.id!=undefined){
            let url = '/admin/getedit';
            axios.post(url,form).then(res=>{
                //console.log(res);
                if(res.data==="ok"){
                    ///history.push("/");
                }
            }).catch(err => {
                console.log(err);
                this.setState({
                    errors: err.response.data.errors
                });
                console.log(this.state);
                this.validate()

            });
        }
        else{
            let url = '/admin/add';
            axios.post(url,form).then(res=>{
                history.push("/");
            }).catch(err => {
                this.setState({
                    errors: err.response.data.errors
                });
                console.log(this.state);
                this.validate()
            });
        }

    }
    validate(){
        if(!_.isEmpty(this.state.errors)){
            console.log("123");
            let listError=this.state.errors;
            let nameError=(listError.name)?listError.name:""
            let emailError=(listError.email)?listError.email:""
            let addressError=(listError.address)?listError.address:""
            let phoneError=(listError.phone)?listError.phone:""

            return(
                <div className="alert alert-danger">
                    <p>{nameError}</p>
                    <p>{emailError}</p>
                    <p>{addressError}</p>
                    <p>{phoneError}</p>

                </div>
            )
        }
    }
    render(){
        const data=this.state.data;
        return(
            <div>
                {this.validate()}
                <form onSubmit={this.handleSubmit}>
                    <input type="hidden" name="id" defaultValue={data.id} />
                    <div className="form-group">
                        <label htmlFor="name">NAME</label>
                        <input type="text" className="form-control" name="name" defaultValue={data.name} onChange={this.change} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">EMAIL</label>
                        <input type="text" className="form-control" name="email" defaultValue={data.email} onChange={this.change} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="address">ADDRESS</label>
                        <input type="text" className="form-control" name="address" defaultValue={data.address} onChange={this.change} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="phone">PHONE</label>
                        <input type="text" className="form-control" name="phone" defaultValue={data.phone} onChange={this.change}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>

        )
    }



}
