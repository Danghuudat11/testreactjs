import React from 'react';
import axios from 'axios';
import {Route, Link} from 'react-router-dom'



export default class ShowComponent extends React.Component{
    constructor(props){
        super(props);
        this.state=({
            data:{}
        });

    }
    componentDidMount() {
        this.fetchData();
    }
    fetchData(){
        let id=this.props.match.params.id;
        let url='/admin/getdetail/'+id;
        axios.get(url).then(res=>{
            this.setState({
                data:res.data.data
            })
        });
    }
    render(){
        const {data} =this.state;
        return(
            <div>
                <table className="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>EMAIL</th>
                        <th>PHONE</th>
                        <th>ADDRESS</th>
                    </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>{data.id}</td>
                            <td>{data.name}</td>
                            <td>{data.email}</td>
                            <td>{data.phone}</td>
                            <td>{data.address}</td>
                        </tr>

                    </tbody>
                </table>
                <div><Link className="btn btn-sm btn-primary" to={'/'}>Back to list</Link></div>
            </div>
        )
    }
}
