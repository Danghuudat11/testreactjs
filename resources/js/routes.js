import React from 'react';
import ListComponent from './components/company/ListComponent'
import ListUserComponent from './components/user/ListComponent'
import ShowComponent from './components/company/ShowComponent'
import FormComponent from "./components/company/FormComponent";
const routes=[
    {
        path: '/',
        exact: true,
        main: () => <ListComponent mode="show" />
    },
    {
        path: '/getdetail/:id',
        exact: false,
        main: ({match}) => <ShowComponent match={match}/>
    },
    {
        path: '/getdelete/:id',
        exact: false,
        main: ({match}) => <ShowComponent mode="delete" match={match}/>
    },
    {
        path: '/getedit/:id',
        exact: false,
        main: ({match}) => <FormComponent mode="edit" match={match}/>
    },
    {
        path: '/getadd',
        exact: false,
        main: ({match}) => <FormComponent mode="add" match={match}/>
    },
    /*{
        path: '/user',
        exact: true,
        main: () => <ListUserComponent />
    },*/
];
export default routes;
