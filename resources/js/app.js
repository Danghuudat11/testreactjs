/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
import ShowComponent from "./components/company/ShowComponent";

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
//import {Provider} from 'react-redux'
import ListComponent from './components/company/ListComponent'
import {
    Router,
    Switch,
    Route} from 'react-router-dom'
import routes from './routes';
import history from './history';

const showRoutes = () => {
    if(routes.length > 0) {
        return routes.map((item, index) => {
            return <Route key={index} path={item.path} exact={item.exact}>{item.main}</Route>;
        })
    }
};

export default class Root extends Component {
    render(){
        return(
            <Router history={history}>
                <Switch >
                    {showRoutes()}
                </Switch>
            </Router>
        )
    }
}
/*if (document.getElementById('app')) {
    ReactDOM.render(<Root />, document.getElementById('app'));
}*/
if (document.getElementById('root')) {
    ReactDOM.render(<Root />, document.getElementById('root'));
}
