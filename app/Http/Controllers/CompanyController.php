<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use Illuminate\Http\Request;
use App\Models\Company;
use Symfony\Component\Console\Input\Input;


class CompanyController extends Controller
{
    public function list(){
        $sort_by=request()->sort_by;
        $type_sort=request()->type_sort;

        $data=Company::orderBy($sort_by,$type_sort)->paginate(10);
        return response()->json([
            'data'=>$data,
        ]);
    }
    public function getDetail($id){
        $data=Company::findOrFail($id);
        return response()->json([
            'data'=>$data,
        ]);
    }
    public function delete($id){
        Company::destroy($id);
        return response()->json([
            'data'=>"ok",
        ]);
    }
    public function edit(Request $request){
        $data=Company::find($request->id);
        $data->update($request->all());
        return "ok";
    }

    public function add(CompanyRequest $request){
        Company::insert($request->all());
        return "ok";
    }

    public function initAdd(){
        $data=Company::initialize();
        return response()->json([
            'data'=>$data,
        ]);

    }

}
