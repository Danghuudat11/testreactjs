<?php


namespace App\Models;


use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use DataTablePaginate;
    protected $table = 'companies';
    protected $fillable = ['email', 'name', 'phone', 'address'];
    protected $hidden = [];
    public static function initialize() {
        return [
            'id'      =>0,
            'email'   => '',
            'name'    => '',
            'phone'   => '',
            'address' => ''
        ];
    }
}
