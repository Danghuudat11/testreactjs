<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
});
Route::prefix('admin')->group(function () {
    Route::get('getlist','CompanyController@list');
    Route::get('getdetail/{id}','CompanyController@getDetail');
    Route::get('getdelete/{id}','CompanyController@delete');
    Route::post('getedit','CompanyController@edit');
    Route::get('getadd','CompanyController@initAdd');
    Route::post('add','CompanyController@add');

});

Route::get('{any}', function () {
    return view('layouts.app');
})->where('any', '.*');

